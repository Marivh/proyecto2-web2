var app = new Vue({
    el: "#app",
    data: {
      username: "",
      password: "",
      services: [],
      service: {},
    },
    created() {},
    methods: {
      //Metodo que valida el usuario y la contraseña y si es correcto obtiene los servicios.
      login() {
        if (
          this.username == USER &&
          this.password == PASS
        ) {
          this.getServices();
        } else {
          var objetivo = document.getElementById('fail');
          objetivo.innerHTML += "El login ha fallado";
        }
      },

      //Método para verificar si ya hay un servicio creado y si no lo hay que cree uno.
      verifyService() {
        if (this.services.length > 0) {
          this.service = this.services[0];
          localStorage.setItem("ServiceId", this.service.sid);
          window.location.replace("./management.html");
        } else {
          this.addService();
        }
      },

      //Método para obtener los servicios.
      getServices() {
        axios({
            method: "get",
            url: "https://chat.twilio.com/v2/Services/",
            auth: {
              username: ACCOUNTSID,
              password: AUTHTOKEN,
            },
          })
          .then((response) => {
            // JSON responses are automatically parsed.
            this.services = response.data.services;
            this.serviceId = services.sid;
            console.log(this.serviceId);
          })
          .catch((e) => {
            // this.errors.push(e);
            console.log(e);
          })
          .finally(() => {
            this.verifyService();
          });
      },

      //Método para agregar un servicio.
      addService() {
        var bodyFormData = new FormData();
        bodyFormData.set("FriendlyName", "MyService");

        axios({
            method: "post",
            url: "https://chat.twilio.com/v2/Services",
            data: bodyFormData,
            headers: {
              "Content-Type": "multipart/form-data"
            },
            auth: {
              username: ACCOUNTSID,
              password: AUTHTOKEN,
            },
          })
          .then((response) => {
            this.services.push(response.data);
          })
          .catch((e) => {
            // this.errors.push(e);
            console.log(e);
          })
          .finally(() => {
            this.verifyService();
          });
      },
    },
  });