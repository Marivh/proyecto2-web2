const Meeting = require("../models/meetingModel");

/**
 * Creates a meeting
 *
 * @param {*} req
 * @param {*} res
 */
const createMeeting = (req, res) => {
    var meeting = new Meeting();

    meeting.title = req.body.title;
    meeting.date = req.body.date;
    meeting.time = req.body.time;
    meeting.users = req.body.users;
    meeting.client = req.body.client;
    meeting.isVirtual = req.body.isVirtual;

    if (meeting.title && meeting.date && meeting.users && meeting.time && meeting.isVirtual) {
        meeting.save(function (err) {
            if (err) {
                res.status(422);
                console.log('Error while saving the meeting', err)
                res.json({
                    error: 'There was an error saving the meeting'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/api/meetings/?id=${meeting.id}`
            });
            res.json(meeting);
        });
    } else {
        res.status(401);
        res.send({
            error: "error while saving the meeting"
        })
        res.json({
            error: 'No valid data provided for meeting'
        });
    }
};

/**
 * Get all meetings
 *
 * @param {*} req
 * @param {*} res
 */
const findByIdMeeting = (req, res) => {
    // if an specific meeting is required
    if (req.query && req.query.id) {
        Meeting.findById(req.query.id, function (err, meeting) {
            if (err) {
                res.status(404);
                console.log('error while queryting the meeting', err)
                res.json({
                    error: "Meeting doesnt exist"
                })
            }
            res.json(meeting);
        });
    } else {
        // get all meetings
        Meeting.find(function (err, meetings) {
            if (err) {
                res.status(422);
                res.json({
                    "error": err
                });
            }
            res.json(meetings);
        });

    }
};

/**
 * Updates a meeting
 *
 * @param {*} req
 * @param {*} res
 */
const updateMeeting = (req, res) => {
    // get meeting by id
    if (req.query.id) {
        Meeting.findById(req.query.id, function (err, meeting) {
            if (err) {
                res.status(404);
                console.log('error while queryting the meeting', err)
                res.json({
                    error: "Meeting doesnt exist"
                })
            }

            // update the meeting object (patch)
            meeting.title = req.body.title ? req.body.title : meeting.title;
            meeting.date = req.body.date ? req.body.date : meeting.date;
            meeting.time = req.body.time ? req.body.time : meeting.time;
            meeting.users = req.body.users ? req.body.users : meeting.users;
            meeting.client = req.body.client ? req.body.client : meeting.client;
            meeting.isVirtual = req.body.isVirtual ? req.body.isVirtual : meeting.isVirtual;

            meeting.save(function (err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the meeting', err)
                    res.json({
                        error: 'There was an error saving the meeting'
                    });
                }
                res.status(200); // OK
                res.json(meeting);
            });
        });
    } else {
        res.status(404);
        res.json({
            error: "Meeting doesnt exist"
        })
    }
};


const deleteMeeting = (req, res) => { //DELETE\
    const id = req.query.id;

    Meeting.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete meeting.Maybe meeting was not found!`
                });
            } else {
                res.send({
                    message: "Meeting was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete meeting with id=" + id
            });
        });
};

module.exports = {
    findByIdMeeting,
    createMeeting,
    updateMeeting,
    deleteMeeting,
}