const Contact = require("../models/contactModel");

/**
 * Creates a contact
 *
 * @param {*} req
 * @param {*} res
 */
const createContact = (req, res) => {
    var contact = new Contact();

    contact.clientID = req.body.clientID;
    contact.name = req.body.name;
    contact.lastname = req.body.lastname;
    contact.email = req.body.email;
    contact.phoneNumber = req.body.phoneNumber;
    contact.position = req.body.position;

    if (contact.clientID && contact.name && contact.lastname && contact.email && contact.phoneNumber && contact.position) {
        contact.save(function (err) {
            if (err) {
                res.status(422);
                console.log('Error while saving the contact', err)
                res.json({
                    error: 'There was an error saving the contact'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/api/contacts/?id=${contact.id}`
            });
            res.json(contact);
        });
    } else {
        res.status(422);
        console.log('No valid data provided for contact')
        res.json({
            error: 'No valid data provided for contact'
        });
    }
};

/**
 * Get all contacts
 *
 * @param {*} req
 * @param {*} res
 */
const findByIdContact = (req, res) => {
    // if an specific contact is required
    if (req.query && req.query.id) {
        Contact.findById(req.query.id, function (err, contact) {
            if (err) {
                res.status(404);
                console.log('error while queryting the contact', err)
                res.json({
                    error: "Contact doesnt exist"
                })
            }
            res.json(contact);
        });
    } else {
        // get all contacts
        Contact.find(function (err, contacts) {
            if (err) {
                res.status(422);
                res.json({
                    "error": err
                });
            }
            res.json(contacts);
        });

    }
};

/**
 * Updates a contact
 *
 * @param {*} req
 * @param {*} res
 */
const updateContact = (req, res) => {
    // get contact by id
    if (req.query.id) {
        Contact.findById(req.query.id, function (err, contact) {
            if (err) {
                res.status(404);
                console.log('error while queryting the contact', err)
                res.json({
                    error: "Contact doesnt exist"
                })
            }

            // update the contact object (patch)
            contact.clientID = req.body.clientID ? req.body.clientID : contact.clientID;
            contact.name = req.body.name ? req.body.name : contact.name;
            contact.lastname = req.body.lastname ? req.body.lastname : contact.lastname;
            contact.email = req.body.email ? req.body.email : contact.email;
            contact.phoneNumber = req.body.phoneNumber ? req.body.phoneNumber : contact.phoneNumber;
            contact.position = req.body.position ? req.body.position : contact.position;

            contact.save(function (err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the contact', err)
                    res.json({
                        error: 'There was an error saving the contact'
                    });
                }
                res.status(200); // OK
                res.json(contact);
            });
        });
    } else {
        res.status(404);
        res.json({
            error: "Contact doesnt exist"
        })
    }
};


const deleteContact = (req, res) => { //DELETE\
    const id = req.query.id;

    Contact.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete contact.Maybe user was not found!`
                });
            } else {
                res.send({
                    message: "User was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete user with id=" + id
            });
        });
};

module.exports = {
    findByIdContact,
    createContact,
    updateContact,
    deleteContact,
}