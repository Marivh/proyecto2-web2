const graphql = require("graphql");

const Client = require("../models/clientModel.js");
const Contact = require("../models/contactModel.js");
const Meeting = require("../models/meetingModel");
const Support = require("../models/supportModel.js");
const Query = require("../models/queryModel.js");

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInt,
    GraphQLBoolean,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull,
} = graphql;

const ClientType = new GraphQLObjectType({
    name: "client",
    fields: () => ({
        id: {
            type: GraphQLID
        },
        name: {
            type: GraphQLString
        },
        legalCertificate: {
            type: GraphQLInt
        },
        webPage: {
            type: GraphQLString
        },
        address: {
            type: GraphQLString
        },
        phoneNumber: {
            type: GraphQLString
        },
        sector: {
            type: GraphQLString
        },
        queries: {
            type: new GraphQLList(OrderType),
            resolve(parent, args) {
                return Query.find({
                    clientID: parent.id
                });
            },
        },
    }),
});

const ContactType = new GraphQLObjectType({
    name: "contact",
    fields: () => ({
        id: {
            type: GraphQLID
        },
        clientID: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        },
        lastName: {
            type: GraphQLString
        },
        email: {
            type: GraphQLString
        },
        phoneNumber: {
            type: GraphQLString
        },
        position: {
            type: GraphQLString
        },
        queries: {
            type: new GraphQLList(OrderType),
            resolve(parent, args) {
                return Query.find({
                    contactID: parent.id
                });
            },
        },
    }),
});

const MeetingType = new GraphQLObjectType({
    name: "meeting",
    fields: () => ({
        id: {
            type: GraphQLID
        },
        title: {
            type: GraphQLString
        },
        date: {
            type: GraphQLString
        },
        time: {
            type: GraphQLString
        },
        email: {
            type: GraphQLString
        },
        users: {
            type: GraphQLObjectType
        },
        client: {
            type: GraphQLString
        },
        isVirtual: {
            type: GraphQLBoolean
        },
        queries: {
            type: new GraphQLList(OrderType),
            resolve(parent, args) {
                return Query.find({
                    meetingID: parent.id
                });
            },
        },
    }),
});

const SupportType = new GraphQLObjectType({
    name: "supporTicket",
    fields: () => ({
        id: {
            type: GraphQLID
        },
        problemTitle: {
            type: GraphQLString
        },
        problemDetail: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        },
        client: {
            type: GraphQLString
        },
        status: {
            type: GraphQLString
        },
        queries: {
            type: new GraphQLList(OrderType),
            resolve(parent, args) {
                return Query.find({
                    supportID: parent.id
                });
            },
        },
    }),
});

const QueryType = new GraphQLObjectType({
    name: "Query",
    fields: () => ({
        id: {
            type: GraphQLID
        },
        client: {
            type: new GraphQLList(ClientType),
            resolve(parent, args) {
                return Client.find({});
            },
        },
        contact: {
            type: new GraphQLList(ContactType),
            resolve(parent, args) {
                return Contact.find({});
            },
        },
        support: {
            type: new GraphQLList(SupportType),
            resolve(parent, args) {
                return Support.find({});
            },
        },
        meeting: {
            type: new GraphQLList(MeetingType),
            resolve(parent, args) {
                return Meeting.find({});
            },
        },
    }),
});

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        clients: {
            type: new GraphQLList(ClientType),
            resolve(parent, args) {
                return Client.find({});
            },
        },
        Contact: {
            type: new GraphQLList(ProductType),
            resolve(parent, args) {
                return Contact.find({});
            },
        },
        product: {
            type: ProductType,
            args: {
                id: {
                    type: GraphQLID
                }
            },
            resolve(parent, args) {
                return Product.findById(args.id);
            },
        },
        orders: {
            type: new GraphQLList(OrderType),
            resolve(parent, args) {
                return Order.find({});
            },
        },
        order: {
            type: OrderType,
            args: {
                id: {
                    type: GraphQLID
                }
            },
            resolve(parent, args) {
                return Order.findById(args.id);
            },
        },
    },
});

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        addClient: {
            type: ClientType,
            args: {
                //GraphQLNonNull make these field required
                name: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                lastName: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                email: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                website: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            resolve(parent, args) {
                let client = new Client({
                    name: args.name,
                    lastName: args.lastName,
                    email: args.email,
                    website: args.website
                });
                return client.save();
            },
        },
        addProduct: {
            type: ProductType,
            args: {
                //GraphQLNonNull make these field required
                name: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                quantity: {
                    type: new GraphQLNonNull(GraphQLInt)
                },
                price: {
                    type: new GraphQLNonNull(GraphQLInt)
                },
            },
            resolve(parent, args) {
                let product = new Product({
                    name: args.name,
                    quantity: args.quantity,
                    price: args.price,
                });
                return product.save();
            },
        },
        addOrder: {
            type: OrderType,
            args: {
                //GraphQLNonNull make these field required
                clientID: {
                    type: new GraphQLNonNull(GraphQLID)
                },
                productsID: {
                    type: new GraphQLNonNull(GraphQLList(GraphQLID))
                },
            },
            resolve(parent, args) {
                let order = new Order({
                    clientID: args.clientID,
                    productsID: args.productsID,
                });
                return order.save();
            },
        },
    },
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation,
});