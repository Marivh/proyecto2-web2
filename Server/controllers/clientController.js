const Client = require("../models/clientModel");

/**
 * Creates a client
 *
 * @param {*} req
 * @param {*} res
 */
const createClient = (req, res) => {
    var client = new Client();

    client.name = req.body.name;
    client.legalCertificate = req.body.legalCertificate;
    client.webPage = req.body.webPage;
    client.address = req.body.address;
    client.phoneNumber = req.body.phoneNumber;
    client.sector = req.body.sector;

    if (client.name && client.legalCertificate && client.webPage && client.address && client.phoneNumber && client.sector) {
        client.save(function (err) {
            if (err) {
                res.status(422);
                console.log('Error while saving the client', err)
                res.json({
                    error: 'There was an error saving the client'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/api/clients/?id=${client.id}`
            });
            res.json(client);
        });
    } else {
        res.status(422);
        console.log('No valid data provided for client')
        res.json({
            error: 'No valid data provided for client'
        });
    }
};

/**
 * Get all clients
 *
 * @param {*} req
 * @param {*} res
 */
const findByIdClient = (req, res) => {
    // if an specific client is required
    if (req.query && req.query.id) {
        Client.findById(req.query.id, function (err, client) {
            if (err) {
                res.status(404);
                console.log('error while queryting the client', err)
                res.json({
                    error: "Client doesnt exist"
                })
            }
            res.json(client);
        });
    } else {
        // get all clients
        Client.find(function (err, clients) {
            if (err) {
                res.status(422);
                res.json({
                    "error": err
                });
            }
            res.json(clients);
        });

    }
};

/**
 * Updates a client
 *
 * @param {*} req
 * @param {*} res
 */
const updateClient = (req, res) => {
    // get client by id
    if (req.query.id) {
        Client.findById(req.query.id, function (err, client) {
            if (err) {
                res.status(404);
                console.log('error while queryting the client', err)
                res.json({
                    error: "Client doesnt exist"
                })
            }

            // update the client object (patch)
            client.name = req.body.name ? req.body.name : client.name;
            client.legalCertificate = req.body.legalCertificate ? req.body.legalCertificate : client.legalCertificate;
            client.webPage = req.body.webPage ? req.body.webPage : client.webPage;
            client.address = req.body.address ? req.body.address : client.address;
            client.phoneNumber = req.body.phoneNumber ? req.body.phoneNumber : client.phoneNumber;
            client.sector = req.body.sector ? req.body.sector : client.sector;

            client.save(function (err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the client', err)
                    res.json({
                        error: 'There was an error saving the client'
                    });
                }
                res.status(200); // OK
                res.json(client);
            });
        });
    } else {
        res.status(404);
        res.json({
            error: "Client doesnt exist"
        })
    }
};


const deleteClient = (req, res) => { //DELETE\
    const id = req.query.id;

    Client.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete client.Maybe user was not found!`
                });
            } else {
                res.send({
                    message: "User was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete user with id=" + id
            });
        });
};

module.exports = {
    findByIdClient,
    createClient,
    updateClient,
    deleteClient,
}