const Support = require("../models/supportModel");

/**
 * Creates a support
 *
 * @param {*} req
 * @param {*} res
 */
const createSupport = (req, res) => {
    var support = new Support();

    support.problemTitle = req.body.problemTitle;
    support.problemDetail = req.body.problemDetail;
    support.name = req.body.name;
    support.client = req.body.client;
    support.status = req.body.status;

    if (support.problemTitle && support.problemDetail && support.name && support.client && support.status) {
        support.save(function (err) {
            if (err) {
                res.status(422);
                console.log('Error while saving the support', err)
                res.json({
                    error: 'There was an error saving the support'
                });
            }
            res.status(201); //CREATED
            res.header({
                'location': `http://localhost:3000/api/supports/?id=${support.id}`
            });
            res.json(support);
        });
    } else {
        res.status(422);
        console.log('error while saving the support')
        res.json({
            error: 'No valid data provided for support'
        });
    }
};

/**
 * Get all supports
 *
 * @param {*} req
 * @param {*} res
 */
const findByIdSupport = (req, res) => {
    // if an specific support is required
    if (req.query && req.query.id) {
        Support.findById(req.query.id, function (err, support) {
            if (err) {
                res.status(404);
                console.log('error while queryting the support', err)
                res.json({
                    error: "Support doesnt exist"
                })
            }
            res.json(support);
        });
    } else {
        // get all supports
        Support.find(function (err, supports) {
            if (err) {
                res.status(422);
                res.json({
                    "error": err
                });
            }
            res.json(supports);
        });

    }
};

/**
 * Updates a support
 *
 * @param {*} req
 * @param {*} res
 */
const updateSupport = (req, res) => {
    // get support by id
    if (req.query.id) {
        Support.findById(req.query.id, function (err, support) {
            if (err) {
                res.status(404);
                console.log('error while queryting the support', err)
                res.json({
                    error: "Support doesnt exist"
                })
            }

            // update the support object (patch)
            support.problemTitle = req.body.problemTitle ? req.body.problemTitle : support.problemTitle;
            support.problemDetail = req.body.problemDetail ? req.body.problemDetail : support.problemDetail;
            support.name = req.body.name ? req.body.name : support.name;
            support.client = req.body.client ? req.body.client : support.client;
            support.status = req.body.status ? req.body.status : support.status;

            support.save(function (err) {
                if (err) {
                    res.status(422);
                    console.log('error while saving the support', err)
                    res.json({
                        error: 'There was an error saving the support'
                    });
                }
                res.status(200); // OK
                res.json(support);
            });
        });
    } else {
        res.status(404);
        res.json({
            error: "Support doesnt exist"
        })
    }
};


const deleteSupport = (req, res) => { //DELETE\
    const id = req.query.id;

    Support.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete support.Maybe support was not found!`
                });
            } else {
                res.send({
                    message: "Support was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete support with id=" + id
            });
        });
};

module.exports = {
    findByIdSupport,
    createSupport,
    updateSupport,
    deleteSupport,
}