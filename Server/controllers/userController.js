const User = require("../models/userModel");

/**
 * Creates a user
 *
 * @param {*} req
 * @param {*} res
 */
const createUser = (req, res) => {
  var user = new User();

  user.name = req.body.name;
  user.lastname = req.body.lastname;
  user.username = req.body.username;
  user.password = req.body.password;
  user.rol = "Usuario";

  if (user.name && user.lastname && user.username && user.password) {
    user.save(function (err) {
      if (err) {
        res.status(422);
        console.log('Error while saving the user', err)
        res.json({
          error: 'There was an error saving the user'
        });
      }
      res.status(201); //CREATED
      res.header({
        'location': `http://localhost:3000/api/users/?id=${user.id}`
      });
      res.json(user);
    });
  } else {
    res.status(422);
    console.log('error while saving the user')
    res.json({
      error: 'No valid data provided for user'
    });
  }
};

/**
 * Get all users
 *
 * @param {*} req
 * @param {*} res
 */
const findById = (req, res) => {
  // if an specific user is required
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({
          error: "User doesnt exist"
        })
      }
      res.json(user);
    });
  } else {
    // get all users
    User.find(function (err, users) {
      if (err) {
        res.status(422);
        res.json({
          "error": err
        });
      }
      res.json(users);
    });

  }
};

/**
 * Updates a user
 *
 * @param {*} req
 * @param {*} res
 */
const updateUser = (req, res) => {
  // get user by id
  if (req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('error while queryting the user', err)
        res.json({
          error: "User doesnt exist"
        })
      }

      // update the user object (patch)
      user.name = req.body.name ? req.body.name : user.name;
      user.lastname = req.body.lastname ? req.body.lastname : user.lastname;
      user.username = req.body.username ? req.body.username : user.username;
      user.password = req.body.password ? req.body.password : user.password;
      // update the user object (put)
      // user.title = req.body.title
      // user.detail = req.body.detail

      user.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the user', err)
          res.json({
            error: 'There was an error saving the user'
          });
        }
        res.status(200); // OK
        res.json(user);
      });
    });
  } else {
    res.status(404);
    res.json({
      error: "User doesnt exist"
    })
  }
};


const deleteUser = (req, res) => { //DELETE\
  const id = req.query.id;

  User.findByIdAndRemove(id)
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete user.Maybe user was not found!`
        });
      } else {
        res.send({
          message: "User was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete user with id=" + id
      });
    });
};

module.exports = {
  findById,
  createUser,
  updateUser,
  deleteUser,
}