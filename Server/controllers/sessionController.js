const Session = require("../models/sessionModel");
var jwt = require('jsonwebtoken')
const crypto = require("crypto");
const User = require("../models/userModel");
const THE_SECRET_KEY = '123';

/**
 * Creates a new session for the user
 *
 * @param {*} username
 */
const saveSession = function (userId, username, rol) {
    var payload = {
        userID: userId,
        username: username,
        rol: rol
    }
    var token = jwt.sign(payload, THE_SECRET_KEY, {
        expiresIn: 60 * 60 * 24 // expires in 24 hours
    })
    // insert token to the session table
    let session = new Session();
    session.token = token;
    session.userID = userId;
    session.username = username;
    session.rol = rol;
    session = session.save();
    return token;
};

/**
 * Delete session
 *
 * @param {*} token
 */
const destroySession = function (token) {
    console.log('token:', token);
    Session.deleteOne({
        token: token
    }, function (err) {
        if (err) {
            console.log('error', err);
        }
        return true;
    });
};

module.exports = {
    saveSession,
    destroySession
}