const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/proyecto-webII");
//Routers
const sessionRouter = require("./routers/session.js");
const { GraphQLServer } = require('graphql-yoga')

const Contact = require("./models/contactModel.js");
const Meeting = require("./models/meetingModel.js");
const Support = require("./models/supportModel.js");


const {
  updateUser,
  createUser,
  findById,
  deleteUser,
} = require("./controllers/userController.js");

const {
  updateClient,
  createClient,
  findByIdClient,
  deleteClient,
} = require("./controllers/clientController.js");

const {
  updateContact,
  createContact,
  findByIdContact,
  deleteContact,
} = require("./controllers/contactController.js");

const {
  updateMeeting,
  createMeeting,
  findByIdMeeting,
  deleteMeeting,
} = require("./controllers/meetingController.js");

const {
  updateSupport,
  createSupport,
  findByIdSupport,
  deleteSupport,
} = require("./controllers/supportController.js");

const typeDefs = `type Query {
  getMeeting: [Meeting]
  getContact: [Contact]
  getSupport:[Support]
}
type Contact {
  clientID: String!
  name: String!
  lastname: String!
  email: String!
  phoneNumber: String!
  position: String!
},
type Meeting {
  title: String!
  date: String!
  time: String!
  users: String!
  client: String!
  isVirtual: String!
},
type Support {
  problemTitle: String!
  problemDetail: String!
  name: String!
  client: String!
  status: String!
}
type User {
  name: String!
  lastname: String!
}`

const resolvers = {
  Query: {
    getContact: () => Contact.find(),
    getMeeting: () => Meeting.find(),
    getSupport: () => Support.find(),
  },
}

const server = new GraphQLServer({
  typeDefs,
  resolvers
})
mongoose.connection.once("open", function () {
  server.start(() => console.log('Server is running on localhost:3000'))
});


// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));

//Listen session request
app.use(sessionRouter);

// listen to the user request
app.get("/api/users", findById);
app.post("/api/users", createUser);
app.patch("/api/users", updateUser);
app.put("/api/users", updateUser);
app.delete("/api/users", deleteUser);

//Listen client request
app.get("/api/clients", findByIdClient);
app.post("/api/clients", createClient);
app.patch("/api/clients", updateClient);
app.put("/api/clients", updateClient);
app.delete("/api/clients", deleteClient);

//Listen contact request
app.get("/api/contacts", findByIdContact);
app.post("/api/contacts", createContact);
app.patch("/api/contacts", updateContact);
app.put("/api/contacts", updateContact);
app.delete("/api/contacts", deleteContact);

//Listen meeting request
app.get("/api/meetings", findByIdMeeting);
app.post("/api/meetings", createMeeting);
app.patch("/api/meetings", updateMeeting);
app.put("/api/meetings", updateMeeting);
app.delete("/api/meetings", deleteMeeting);

//Listen support request
app.get("/api/supports", findByIdSupport);
app.post("/api/supports", createSupport);
app.patch("/api/supports", updateSupport);
app.put("/api/supports", updateSupport);
app.delete("/api/supports", deleteSupport);

app.listen(3000, () => console.log(`
Example app listening on port 3000!`))