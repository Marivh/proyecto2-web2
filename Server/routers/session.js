var jwt = require('jsonwebtoken');
const Session = require("../models/sessionModel");
const User = require("../models/userModel");
const express = require('express')
const bcrypt = require('bcrypt');
const THE_SECRET_KEY = 'SecretKey';

const router = express.Router()

const {
    saveSession,
    destroySession
} = require("../controllers/sessionController.js");


/**
 * Login for a token based authentication
 */
router.post("/api/session", function (req, res) {
    username = req.body.username;
    password = req.body.password;
    rol = "";
    if (username && password) {
        User.findOne({
            username: req.body.username
        }, function (err, userDB) {
            if (err) {
                next(err);
            } else {
                if (username == userDB.username) {
                    if (bcrypt.compare(password, userDB.password)) {
                        var token = saveSession(userDB._id, username, userDB.rol);
                        if (userDB.rol == "Administrador") {
                            rol = "Administrador";
                        } else {
                            rol = "Usuario";
                        }
                        res.send({
                            token,
                            rol
                        })
                    } else {
                        res.status(401);
                        res.send({
                            err: "Invalid cretentials"
                        })
                    }
                } else {
                    res.status(401);
                    res.send({
                        err: "Invalid cretentials"
                    })
                }
            }
        });
    } else {
        res.status(400);
        res.send({
            err: "Bad request"
        })
    }
});

/**
 * Logout session
 */
router.delete("/api/session", function (req, res) {
    if (req.headers["authorization"]) {
        const token = req.headers['authorization'].split(' ')[1];
        // insert token to the session table
        destroySession(token);
        res.status(204).send({});
    } else {
        res.status(401);
        res.send({
            error: "Unauthorized "
        });
    }
});

// Token based Auth Middleware
router.use(function (req, res, next) {
    if (req.headers["authorization"]) {
        const authtoken = req.headers['authorization'].split(' ')[1];
        try {
            jwt.verify(authtoken, '123', (err, decodedToken) => {
                if (err || !authtoken) {
                    res.status(401);
                    res.json({
                        error: authtoken
                    });
                }
                console.log(authtoken);
                console.log('Payload:', decodedToken);
                next();
            });
        } catch (e) {
            res.status(401);
            res.send({
                error: "Unauthorized"
            });
        }
    }
});

module.exports = router;