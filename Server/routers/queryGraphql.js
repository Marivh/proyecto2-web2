const express = require('express')
const router = express.Router()
const mongoose = require("mongoose");

const { GraphQLServer } = require('graphql-yoga');

const typeDefs = `type Query {
    getClient: [Client]
    getMeeting: [Client]
    getContact: [Client]
    getSupport:[Client]
  }
  type Client {
    name: String!
    legalCertificate: String!
    webPage: String!
    address: String!
    phoneNumber: String!
    sector: String!
  }`

const resolvers = {
    Query: {
        getClient: () => Client.find(),
        getContact: () => Contact.find(),
        getMeeting: () => Meeting.find(),
        getSupport: () => Support.find(),
    },
}

const server = new GraphQLServer({
    typeDefs,
    resolvers
})

router.use(mongoose.connection.once("open", function () {
    server.start(() => console.log('Server is running on localhost:4000'))
}));