const typeDefs = `type Query {
    getClient: [Client]
    getMeeting: [Client]
    getContact: [Client]
    getSupport:[Client]
  }
  type Client {
    name: String!
    legalCertificate: String!
    webPage: String!
    address: String!
    phoneNumber: String!
    sector: String!
  }`

const resolvers = {
    Query: {
        getClient: () => Client.find(),
        getContact: () => Contact.find(),
        getMeeting: () => Meeting.find(),
        getSupport: () => Support.find(),
    },
}

module.exports = {
    typeDefs,
    resolvers
}