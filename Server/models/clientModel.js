// Cargamos el módulo de mongoose
const mongoose = require('mongoose');
//Definimos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema con sus correspondientes campos
const client = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
    },
    legalCertificate: {
        type: Number,
        trim: true,
        required: true
    },
    webPage: {
        type: String,
        trim: true,
        required: true
    },
    address: {
        type: String,
        trim: true,
        required: true
    },
    phoneNumber: {
        type: String,
        trim: true,
        required: true
    },
    sector: {
        type: String,
        trim: true,
        required: true
    }
});

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('clients', client);