const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const session = new Schema({
    token: {
        type: String
    },
    userID: {
        type: String
    },
    username: {
        type: String
    },
    rol: {
        type: String
    }
});


module.exports = mongoose.model('sessions', session);