// Cargamos el módulo de mongoose
const mongoose = require('mongoose');
//Definimos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema con sus correspondientes campos
const meeting = new Schema({
    title: {
        type: String,
        trim: true,
        required: true,
    },
    date: {
        type: Date,
        trim: true,
        required: true
    },
    time: {
        type: String,
        trim: true,
        required: true
    },
    users: {
        type: Object,
        trim: true,
        required: true,
    },
    client: {
        type: String,
        trim: true
    },
    isVirtual: {
        type: Boolean,
        trim: true,
        required: true
    }
});

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('meetings', meeting);