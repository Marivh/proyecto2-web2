// Cargamos el módulo de mongoose
const mongoose = require('mongoose');
//Definimos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema con sus correspondientes campos
const support = new Schema({
    problemTitle: {
        type: String,
        trim: true,
        required: true,
    },
    problemDetail: {
        type: String,
        trim: true,
        required: true
    },
    name: {
        type: String,
        trim: true,
        required: true,
    },
    client: {
        type: String,
        trim: true,
        required: true
    },
    status: {
        type: String,
        trim: true,
        required: true,
    }
});

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model('supports', support);